
-- SUMMARY --

Module provides widget to sent emails from amoCRM. It provides the modal with form, when user fills the following info:
* To (recipient)
* Subject (plain text)
* Body (plain text / HTML)

-- CONFIGURATION --

If the "Body" field of form is not ckeditor-field, please follow the instructions to set  up the HTML in the emails
https://www.drupal.org/node/1200142.
If you don't want to use HTML in the emails then additional configuration is not needed.
