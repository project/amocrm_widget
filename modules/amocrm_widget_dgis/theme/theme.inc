<?php
/**
 * @file
 * Theme function of amoCRM 2gis widget.
 */

/**
 * Preprocess for theming amocrm_widget_dgis_company_teaser.
 */
function amocrm_widget_dgis_preprocess_amocrm_widget_dgis_company_teaser(&$vars) {
  if (!empty($vars['data']['name']) && !empty($vars['data']['id']) && !empty($vars['data']['hash'])) {
    $vars['company_title'] = l(
      t($vars['data']['name']),
      'amocrm_widget/embed',
      array(
        'attributes' => array(
          'id' => 'drupal-2gis-link-firm',
          'class' => array('drupal-2gis-link', 'widget-2gis-list-item-title')
        ),
        'query' => array('url' => '2gis-api/amocrm-widget/' . $vars['data']['id'] . '/' . $vars['data']['hash'])
      )
    );
  }
}

/**
 * Preprocess for theming amocrm_widget_dgis_company_teaser_wrapper.
 */
function amocrm_widget_dgis_preprocess_amocrm_widget_dgis_company_teaser_wrapper(&$vars) {
  $change_city = l(
    t('Change city'),
    'amocrm_widget/embed',
    array(
      'attributes' => array('id' => 'drupal-2gis-link-city', 'class' => array('drupal-2gis-link, widget-2gis-change-city')),
      'query' => array('url' => '2gis-api/amocrm-widget/city-change')
    )
  );
  if (!empty($_SESSION['amocrm_context']) && !empty($_SESSION['amocrm_context']['dgis_city'])) {
    $city = $_SESSION['amocrm_context']['dgis_city'];
  }
  else {
    $city = variable_get('dgis_city', DGIS_DEFAULT_CITY);
  }

  $vars['data']['summary'] = t(
    'Found !count organisations in the city !city !change_city',
    array(
      '!count' => $vars['data']['total'],
      '!city' => $city,
      '!change_city' => $change_city
    )
  );
}

/**
 * Preprocess for theming amocrm_widget_dgis_wrapper.
 */
function amocrm_widget_dgis_preprocess_amocrm_widget_dgis_wrapper(&$vars) {
  // Settings for home link.
  $settings = array(
    'pageType' => $_SESSION['amocrm_context']['card_entity'],
    'context' => $_SESSION['amocrm_context'],
  );
  drupal_add_js(array('amoCRMWidget' => $settings), 'setting');

  $logo = theme('image', array('path' => amocrm_widget_dgis_MODULE_PATH . '/images/2gis-logo.png'));
  $dgis_site_external = variable_get('dgis_site_external', 'http://2gis.ru');
  if ($dgis_site_external) {
    $logo = l(
      $logo,
      $dgis_site_external,
      array(
        'external' => TRUE,
        'html' => TRUE,
        'attributes' => array('class' => array('widget-2gis-logo'), 'target' => '_blank'),
      )
    );
  }
  $vars['logo'] = $logo;
}

/**
 * Preprocess for theming amocrm_widget_dgis_company_full.
 */
function amocrm_widget_dgis_preprocess_amocrm_widget_dgis_company_full(&$vars) {
  if (!empty($vars['data']['contacts'])) {
    $vars['contacts'] = array();
    foreach ($vars['data']['contacts'] as $group) {
      foreach ($group['contacts'] as $contact) {
        $group['name'] = $group['name'] ? $group['name'] : 'default';

        $websites = array('website', 'vkontakte', 'instagram', 'twitter', 'facebook');
        if (in_array($contact['type'], $websites)) {
        $contact['value'] = l($contact['alias'], $contact['value'], array(
          'external' => TRUE,
          'attributes' => array('target'=>'_blank'),
        ));
      }
        switch($contact['type']) {
          case 'fax':
          case 'phone':
            $contact['type'] = 'phone';
            break;

          case 'website':
          case 'vkontakte':
          case 'twitter':
          case 'facebook':
          case 'instagram':
            $contact['type'] = 'site';
            break;

          default:
            $contact['type'] = 'address';
        }
        $vars['contacts'][$group['name']][$contact['type']][] = $contact['value'];
      }
    }
  }
  if (!empty($vars['show_choose_link'])) {
    $vars['choose_link'] = l(
      '+ <span>' . t('Connect with 2GIS') . '</span>',
      'amocrm_widget/embed',
      array(
        'attributes' => array('id' => 'drupal-2gis-link-firm', 'class' => array('drupal-2gis-link')),
        'query' => array('url' => '2gis-api/amocrm-widget/connect/' . $vars['data']['id'] . '/' . $vars['hash']),
        'html' => TRUE,
      )
    );
    $vars['back_link'] = l(
      '← <span>' . t('Return') . '</span>',
      'amocrm_widget/embed',
      array(
        'attributes' => array('id' => 'drupal-2gis-link-back', 'class' => array('drupal-2gis-link')),
        'query' => array('url' => '2gis-api/amocrm-widget'),
        'html' => TRUE,
      )
    );
  }
  else {
    $vars['choose_link'] = l(
      '+ <span>' . t('Synchronise with 2GIS') . '</span>',
      'amocrm_widget/embed',
      array(
        'attributes' => array('id' => 'drupal-2gis-link-firm', 'class' => array('drupal-2gis-link')),
        'query' => array('url' => '2gis-api/amocrm-widget/synchronise'),
        'html' => TRUE,
      )
    );
  }
}
