<?php
/**
 * @file
 * Module amoCRM widget 2gis.
 */

require_once 'amocrm_widget_dgis.functions.inc';
require_once 'amocrm_widget_dgis.pages.inc';

define('amocrm_widget_dgis_MODULE_PATH', drupal_get_path('module', 'amocrm_widget_dgis'));

/**
 * Implements hook_menu().
 */
function amocrm_widget_dgis_menu() {
  $items['2gis-api/amocrm-widget'] = array(
    'title' => 'amoCRM widget 2gis',
    'page callback' => 'amocrm_widget_dgis_callback',
    'access callback' => TRUE,
  );
  $items['2gis-api/amocrm-widget/%/%'] = array(
    'title' => 'amoCRM widget 2gis: company view',
    'page callback' => 'amocrm_widget_dgis_company_callback',
    'page arguments' => array(2,3),
    'access callback' => TRUE,
  );
  $items['2gis-api/amocrm-widget/connect/%/%'] = array(
    'title' => 'amoCRM widget 2gis: company connect',
    'page callback' => 'amocrm_widget_dgis_company_connect',
    'page arguments' => array(3,4),
    'access callback' => TRUE,
  );
  $items['2gis-api/amocrm-widget/remove'] = array(
    'title' => 'amoCRM widget remove 2gis',
    'page callback' => 'amocrm_widget_dgis_remove_callback',
    'access callback' => TRUE,
  );
  $items['2gis-api/amocrm-widget/connected-entity'] = array(
    'title' => 'amoCRM widget remove 2gis',
    'page callback' => 'amocrm_widget_dgis_connected_entity_callback',
    'access callback' => TRUE,
  );
  $items['2gis-api/amocrm-widget/synchronise'] = array(
    'title' => 'amoCRM widget remove 2gis',
    'page callback' => 'amocrm_widget_dgis_synchronise_callback',
    'access callback' => TRUE,
  );
  $items['2gis-api/amocrm-widget/city-change'] = array(
    'title' => 'amoCRM widget change city',
    'page callback' => 'amocrm_widget_dgis_change_city_callback',
    'access callback' => TRUE,
  );
  $items['2gis-api/amocrm-widget/city-change/%'] = array(
    'title' => 'amoCRM widget change city',
    'page callback' => 'amocrm_widget_dgis_set_city_callback',
    'page arguments' => array(3),
    'access callback' => TRUE,
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function amocrm_widget_dgis_init() {
  if (!variable_get('amocrm_widget_dgis_linking_field_id', FALSE) && user_access('administer modules')) {
    drupal_set_message(
      t('Module "amoCRM widget 2gis" requires to be installed when module "amoCRM widget" is well configured with
        API key. Please make sure that configuration is done and re-install module "amoCRM widget 2gis"'),
      'error'
    );
  }

  // Get the widget preloader.
  $preloader = theme('amocrm_widget_preload');
  $output = theme('amocrm_widget_page', array('content' => $preloader));
  drupal_add_js(array('DgisAmoCRM' => array('preloader_page' => $output)), 'setting');

  drupal_add_js(drupal_get_path('module', 'amocrm_widget_dgis') . '/includes/amocrm_widget_dgis.js');
}

/**
 * Implements hook_theme().
 */
function amocrm_widget_dgis_theme($existing, $type, $theme, $path) {
  $base = array(
    'path' => $path . '/theme',
    'file' => 'theme.inc',
  );

  $items['amocrm_widget_dgis_company_teaser'] = $base + array(
      'template' => 'amocrm-widget-dgis-company-teaser',
      'variables' => array('data' => NULL),
    );
  $items['amocrm_widget_dgis_company_full'] = $base + array(
      'template' => 'amocrm-widget-dgis-company-full',
      'variables' => array('data' => NULL),
    );
  $items['amocrm_widget_dgis_company_teaser_wrapper'] = $base + array(
      'template' => 'amocrm-widget-dgis-company-teaser-wrapper',
      'variables' => array('data' => NULL),
    );
  $items['amocrm_widget_dgis_city_change_wrapper'] = $base + array(
      'template' => 'amocrm-widget-dgis-city-change-wrapper',
      'variables' => array('cities' => array()),
    );
  $items['amocrm_widget_dgis_wrapper'] = $base + array(
      'template' => 'amocrm-widget-dgis-wrapper',
      'variables' => array('content' => ''),
    );

  return $items;
}

/**
 * Implements hook_amocrm_widget_info().
 */
function amocrm_widget_dgis_amocrm_widget_info() {
  $content = l(
    t('Connect with 2gis'),
    'amocrm_widget/embed',
    array(
      'attributes' => array('id' => 'drupal-2gis-link', 'class' => array('btn', 'btn-default', 'btn-amo-widget', 'drupal-dgis-connect-button')),
      'query' => array('url' => '2gis-api/amocrm-widget')
    )
  );
  if ($dgis_company = amocrm_widget_dgis_get_cached_data()) {
    $content = l(
      t('Connected with 2gis'),
      'amocrm_widget/embed',
      array(
        'attributes' => array('id' => 'drupal-2gis-link', 'class' => array('btn', 'btn-default', 'btn-amo-widget', 'drupal-dgis-connect-button')),
        'query' => array('url' => '2gis-api/amocrm-widget/connected-entity')
      )
    );
    $content .= l(
      t('Remove connection'),
      'amocrm_widget/embed',
      array(
        'attributes' => array('id' => 'drupal-2gis-remove-link', 'class' => array('btn', 'btn-default', 'btn-amo-widget', 'drupal-dgis-connect-button')),
        'query' => array('url' => '2gis-api/amocrm-widget/remove')
      )
    );
  }

  return array(
    array(
      'title' => t('2gis widget'),
      'description' => '',
      'settings_form_path' => '',
      'links' => array(
        array(
          'content' => '<div class="widget-2gis-initial-page">' . $content . '</div>',
          'group' => t('2GIS'),
          'amocrm_page' => AMOCRM_TYPE_CONTACT,
        ),
      ),
    ),
    array(
      'title' => t('2gis widget'),
      'description' => '',
      'settings_form_path' => '',
      'links' => array(
        array(
          'content' => '<div class="widget-2gis-initial-page">' . $content . '</div>',
          'group' => t('2GIS'),
          'amocrm_page' => AMOCRM_TYPE_COMPANY,
        ),
      ),
    ),
  );
}
