<?php
/**
 * @file
 * Page callbacks for module amoCRM widget 2gis.
 */

/**
 * Page callback for 2gis-api/amocrm-widget.
 */
function amocrm_widget_dgis_callback() {
  $search_fields = amocrm_widget_dgis_get_fields_for_search();
  $data = array();
  foreach ($search_fields as $fields) {
    if (!empty($fields)) {
      if (!empty($_SESSION['amocrm_context']['dgis_city'])) {
        $city = $_SESSION['amocrm_context']['dgis_city'];
      } else {
        $city = variable_get('dgis_city', DGIS_DEFAULT_CITY);
      }
      foreach ($fields as $field) {
        $result = dgis_get_firms(check_plain($field), 'relevance', 1, 20, FALSE, FALSE, $city);
        if (!empty($result)) {
          foreach ($result as $firm) {
            $data[$firm['id']] = $firm;
          }
        }
      }
    }
  }
  $return = '';
  $return_count = count($data);
  if (!empty($data)) {
    foreach ($data as $item) {
      $return .= theme('amocrm_widget_dgis_company_teaser', array('data' => $item));
    }
  }
  $output = theme(
    'amocrm_widget_dgis_company_teaser_wrapper',
    array(
      'data' => array('results' => $return, 'total' => $return_count)
    )
  );

  return theme('amocrm_widget_dgis_wrapper', array('content' => $output));
}

/**
 * Callback for 2gis-api/amocrm-widget/remove.
 */
function amocrm_widget_dgis_remove_callback() {
  $res = t('Error with removing of connection');
  $entity = amocrm_widget_dgis_get_amocrm_entities_from_context();
  if (empty($entity)) {
    return $res;
  }

  $current_entity = $entity['data'];
  $card_entity = $entity['card_entity'];
  $client = amocrm_api_get_default_client();

  switch($card_entity) {
    case AMOCRM_TYPE_CONTACT:
      if (amocrm_widget_dgis_remove_prepare($current_entity)) {
        amocrm_api_contacts_update($client, array($current_entity));
        $res = t('Connection was successfully removed');
      }
      if (!empty($current_entity['linked_company'])) {
        $linked_company = $current_entity['linked_company'];
        if (!empty($linked_company)) {
          if (amocrm_widget_dgis_remove_prepare($linked_company)) {
            amocrm_api_companies_update($client, array($linked_company));
            $res = t('Connection was successfully removed');
          }
        }
      }
      break;

    case AMOCRM_TYPE_COMPANY:
      if (amocrm_widget_dgis_remove_prepare($current_entity)) {
        amocrm_api_companies_update($client, array($current_entity));
        $res = t('Connection was successfully removed');
      }
      break;
  }
  module_invoke_all('amocrm_widget_dgis_delete_reference', $entity['data']['linked_company_id']);

  $content = '<div class="widget-2gis-ico-status widget-2gis-ico-status-success"></div>
          <div class="widget-2gis-ico-status-text">' . $res . '</div>';

  return theme('amocrm_widget_dgis_wrapper', array('content' => $content));
}

/**
 * Callback for 2gis-api/amocrm-widget/connected_entity.
 */
function amocrm_widget_dgis_connected_entity_callback() {
  $cached_dgis_data = amocrm_widget_dgis_get_cached_data();
  if (!empty($cached_dgis_data)) {
    $content =  theme('amocrm_widget_dgis_company_full', array(
      'data' => $cached_dgis_data['data'],
    ));
    return theme('amocrm_widget_dgis_wrapper', array('content' => $content));
  }

  return t('Error with getting connected company');
}

/**
 * Callback for 2gis-api/amocrm-widget/synchronise.
 */
function amocrm_widget_dgis_synchronise_callback() {
  $error = t('Error with synchronisation');
  $cached_dgis_data = amocrm_widget_dgis_get_cached_data();
  $entity = amocrm_widget_dgis_get_amocrm_entities_from_context();
  if (empty($entity)) {
    return $error;
  }
  $current_entity = $entity['data'];
  $card_entity = $entity['card_entity'];

  if ($card_entity != AMOCRM_TYPE_CONTACT) {
    return t('You should do this operation from the contact page.');
  }
  $dgis_company = $cached_dgis_data['data'];
  $dgis_company_id = $cached_dgis_data['id'];
  $hash = $cached_dgis_data['hash'];
  if ($data = dgis_get_firm_profile($dgis_company_id, $hash)) {
    if (amocrm_widget_dgis_save_connection($current_entity, array('id' => $dgis_company_id, 'hash' => $hash, 'data' => $data))) {
      $content = '<div class="widget-2gis-ico-status widget-2gis-ico-status-success"></div>
          <div class="widget-2gis-ico-status-text">' . t('Company was successfully connected. Please reload the page to see the changes.') . '</div>';

      return theme('amocrm_widget_dgis_wrapper', array('content' => $content));
    }
  }
  else {
    // Try to find the company by name again.
    $result = dgis_get_firms($dgis_company['name']);
    if (!empty($result)) {
      $return = '';
      foreach ($result as $item) {
        $return .= theme('amocrm_widget_dgis_company_teaser', array('data' => $item));
      }
      return $return;
    }
    return t('No results were found.');
  }

  return $error;
}

/**
 * Page callback for 2gis-api/amocrm-widget/%.
 */
function amocrm_widget_dgis_company_callback($dgis_company_id, $hash) {
  $content = t('Company was not found, please choose another company');
  if (empty($dgis_company_id) || empty($hash)) {
    return $content;
  }
  if ($data = dgis_get_firm_profile($dgis_company_id, $hash)) {
    $content = theme('amocrm_widget_dgis_company_full', array('data' => $data, 'hash' => $hash, 'show_choose_link' => TRUE));
  }

  return theme('amocrm_widget_dgis_wrapper', array('content' => $content));
}

/**
 * Callback for 2gis-api/amocrm-widget/connect/%/%.
 */
function amocrm_widget_dgis_company_connect($dgis_company_id, $hash) {
  if (!variable_get('amocrm_widget_dgis_linking_field_id', FALSE)) {
    return t('Internal problems with module installation are faced. Please contact the administrator of your Drupal site.');
  }
  $error = t('Error with connection');
  if (empty($dgis_company_id) || empty($hash)) {
    return $error;
  }
  if ($data = dgis_get_firm_profile($dgis_company_id, $hash)) {
    $entity = amocrm_widget_dgis_get_amocrm_entities_from_context();
    if (empty($entity)) {
      return $error;
    }
    $current_entity = $entity['data'];
    $card_entity = $entity['card_entity'];

    if ($card_entity != AMOCRM_TYPE_CONTACT) {
      return t('You should do this operation from the contact page.');
    }

    if (amocrm_widget_dgis_save_connection($current_entity, array('id' => $dgis_company_id, 'hash' => $hash, 'data' => $data))) {
      $content = '<div class="widget-2gis-ico-status widget-2gis-ico-status-success"></div>
          <div class="widget-2gis-ico-status-text">' . t('Connection with 2GIS was successful') . '</div>';

      return theme('amocrm_widget_dgis_wrapper', array('content' => $content));
    }
  }

  return $error;
}

/**
 * Callback for 2gis-api/amocrm-widget/city-change.
 */
function amocrm_widget_dgis_change_city_callback() {
  $cities = dgis_get_cities();
  if (empty($cities)) {
    return t('No cities were found');
  }
  $items = array();
  foreach ($cities as $city) {
    $link = l(
      $city['name']. ' (' . $city['code'] . ')',
      '2gis-api/amocrm-widget/city-change/' . $city['name'],
      array(
        'attributes' => array(
          'id' => 'drupal-2gis-link-city-choose',
          'class' => array('drupal-2gis-link')
        ),
      )
    );
    $items[] = array('data' => $link, 'class' => array('widget-2gis-list-item'));
  }
  $return = theme('item_list', array(
    'items' => $items,
    'type' => 'ul',
    'attributes' => array('class' => array('widget-2gis-list-wrap'))
  ));
  $return = theme('amocrm_widget_dgis_city_change_wrapper', array('cities' => $return));

  return theme('amocrm_widget_dgis_wrapper', array('content' => $return));
}

/**
 * Callback for 2gis-api/amocrm-widget/city-change/%.
 */
function amocrm_widget_dgis_set_city_callback($city) {
  if (!empty($city)) {
    $_SESSION['amocrm_context']['dgis_city'] = $city;
  }
  drupal_goto(
    'amocrm_widget/embed',
    array(
      'query' => array('url' => '2gis-api/amocrm-widget')
    )
  );
}
