(function ($) {

  Drupal.behaviors.amocrmWidgetTrello = {
    attach: function (context, settings) {
      $('#drupal-create-board').once('trello-create-board-button', function(){
        $(this).on('click', function(element) {
          var options = {
            url: Drupal.settings.basePath + 'amocrm-widget-trello/create-board-form',
            submit: {

            }
          };
          var ajax = new Drupal.ajax(false, false, options);
          ajax.eventResponse(ajax, {});

          return false;
        });
      });
      $('#drupal-create-card').once('trello-create-board-button', function(){
        $(this).on('click', function(element) {
          initPreloader();
          var options = {
            url: Drupal.settings.basePath + 'amocrm-widget-trello/create-card-form',
            submit: {

            }
          };
          var ajax = new Drupal.ajax(false, false, options);
          ajax.eventResponse(ajax, {});

          return false;
        });
      });
    }
  };

})(jQuery);
