<?php
/**
 * @file
 * Main template for trello widget.
 */
?>

<div class="widget-trello">
  <?php if (!empty($card_name)): ?>
  <div class="widget-trello-profile">
      <?php if (!empty($labels)):?>
        <ul class="widget-trello-profile__tags">
          <?php foreach ($labels as $label): ?>
            <li class="widget-trello-profile__tag widget-trello-profile__tag_color_<?php print $label; ?>"></li>
          <?php endforeach; ?>
        </ul>
      <?php endif;?>
    <div class="widget-trello-profile__header"><a href="<?php print $url; ?>" target="_blank"><?php print $card_name; ?></a></div>
    <ul class="widget-trello-profile__info-tags">
      <li class="widget-trello-profile__info-tag widget-trello-profile__info-tag_type_text"></li>
      <?php if (!empty($comment_count)):?>
        <li class="widget-trello-profile__info-tag widget-trello-profile__info-tag_type_comment">
          <?php print $comment_count; ?>
        </li>
      <?php endif;?>
      <?php if (!empty($check_list)):?>
        <li class="widget-trello-profile__info-tag widget-trello-profile__info-tag_type_tasks">
          <?php print $check_list; ?>
        </li>
      <?php endif;?>
      <?php if (!empty($due_date)):?>
        <li class="widget-trello-profile__info-tag widget-trello-profile__info-tag_type_date widget-trello-profile__info-tag_status_fail">
          <?php print $due_date; ?>
        </li>
      <?php endif;?>
    </ul>
    <?php if (!empty($members)): ?>
      <ul class="widget-trello-profile__users">
        <?php foreach ($members as $member): ?>
        <li class="widget-trello-profile__user">
          <a href="<?php print $member['url']; ?>" target="_blank"><img src="<?php print $member['path']; ?>" alt="<?php print $member['title']; ?>" class="widget-trello-profile__photo"></a>
        </li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>
    <div class="clear"></div>
  </div>
  <?php if (!empty($board)): ?>
    <ul class="widget-trello-tree">
      <li class="widget-trello-tree__item"><a href="<?php print $board['url']; ?>" target="_blank" class="widget-trello-tree__link" title="<?php print $board['name']; ?>"><?php print $board['name']; ?></a></li>
    </ul>
  <?php endif; ?>

  <?php endif; ?>


  <div class="widget-trello-footer">
    <a href="#" id="drupal-create-board" class="widget-trello-footer__btn <?php if (!empty($card_name)): ?> widget-trello-footer__btn_status_disabled <?php endif; ?>">
      <?php print t('Create Board'); ?>
    </a>
    <a href="#" id="drupal-create-card" class="widget-trello-footer__btn <?php if (!empty($card_name)): ?> widget-trello-footer__btn_status_disabled <?php endif; ?>">
      <?php print t('Create Card'); ?>
    </a>
  </div>
</div>
