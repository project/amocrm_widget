<?php

/**
 * @file
 * Theme functions for amocrm_widget_trello module.
 */

/**
 * Process variables for amocrm-widget-trello-widget.tpl.php.
 */
function template_preprocess_amocrm_widget_trello_widget(&$vars) {
  if (!empty($vars['options']['labels'])) {
    foreach ($vars['options']['labels'] as $label) {
      $vars['labels'][] = "$label";
    }
  }

  if (!empty($vars['options']['url'])) {
    $vars['url'] = $vars['options']['url'];
  }

  if (!empty($vars['options']['name'])) {
    $vars['card_name'] = $vars['options']['name'];
  }

  if (!empty($vars['options']['comments'])) {
    $vars['comment_count'] = $vars['options']['comments'];
  }

  if (!empty($vars['options']['check_list'])) {
    $vars['check_list'] = $vars['options']['check_list'];
  }

  if (!empty($vars['options']['due'])) {
    $date = format_date(strtotime($vars['options']['due']), 'custom', 'd M');
    $vars['due_date'] = $date;
  }

  if (!empty($vars['options']['members'])) {
    foreach ($vars['options']['members'] as $member) {
      $avatar = array(
        'path' => $member['avatar'],
        'title' => $member['name'],
        'url' => $member['url'],
      );
      $vars['members'][] = $avatar;
    }
  }

  if (!empty($vars['options']['board'])) {
    $board = array(
      'name' => $vars['options']['board']['name'],
      'url' => $vars['options']['board']['url'],
    );
    $vars['board'] = $board;
  }
}
