<?php

/**
 * @file
 * amoCRM Widget Trello API code.
 *
 * This file contains the code that interacts with the API.
 */

include_once drupal_get_path('module', 'trello') . '/trello.inc';

/**
 * Class TrelloWidget - create board, lists, card. Get information about card.
 */
class TrelloWidget extends Trello {
  /**
   * @param string $name - name of new board.
   * @param array $options - optional, valid values:
   *   desc - A string with a length from 0 to 16384.
   *   idOrganization - The id or name of the organization to add the board to.
   *   idBoardSource - The id of the board to copy into the new board.
   *   keepFromSource - Components of the source board to copy (default: all).
   *   powerUps - All or a comma-separated list of: calendar, cardAging, recap, voting.
   *   prefs_permissionLevel - One of: org, private, public (default: private).
   *   prefs_voting - One of: disabled, members, observers, org, public (default: disabled).
   *   prefs_comments - One of: disabled, members, observers, org, public (default: members).
   *   prefs_invitations - One of: admins, members (default: members).
   *   prefs_selfJoin - True or false (default: true).
   *   prefs_cardCovers - True or false (default: true).
   *   prefs_background - A string with a length from 0 to 16384 (default: blue).
   *   prefs_cardAging - One of: pirate, regular (default: regular).
   * See more: https://developers.trello.com/advanced-reference/board#post-1-boards
   */
  public function createBoard($name, $options = array()) {
    $args = array(
      'name' => $name,
    );
    if (!empty($options) && is_array($options)) {
      $args = array_merge($args, $options);
    }
    $this->post('boards', $args);
  }

  /**
   * @param string $name - name of new list.
   * @param string $idBoard - id of board for new list.
   * @param array $options - optional, valid values:
   *  idListSource - The id of the list to copy into a new list.
   *  pos - A position. top, bottom, or a positive number (default: top).
   * See more: https://developers.trello.com/advanced-reference/list#post-1-lists
   */
  public function createList($name, $idBoard, $options = array()) {
    $args = array(
      'name' => $name,
      'idBoard' => $idBoard,
    );
    if (!empty($options) && is_array($options)) {
      $args = array_merge($args, $options);
    }
    $this->post('lists', $args);
  }

  /**
   * @param $due - a date, or null.
   * @param $idList - id of the list that the card should be added to.
   * @param $urlSource - a URL starting with http:// or https:// or null.
   * @param array $options - optional, valid values:
   *   name - The name of the new card. It isn't required if the name is being copied from provided by a URL, file or card that is being copied.
   *   desc - a string with a length from 0 to 16384.
   *   pos - A position. top, bottom, or a positive number (default: bottom).
   *   idMembers - A comma-separated list of objectIds, 24-character hex strings.
   *   idLabels - A comma-separated list of objectIds, 24-character hex strings.
   *   fileSource - A file.
   *   idCardSource - The id of the card to copy into a new card.
   *   keepFromSource - Properties of the card to copy over from the source (default: all).
   * See more: https://developers.trello.com/advanced-reference/card#post-1-cards
   */
  public function createCard($due, $idList, $urlSource, $options = array()) {
    $args = array(
      'due' => $due,
      'idList' => $idList,
      'urlSource' => $urlSource,
    );
    if (!empty($options) && is_array($options)) {
      $args = array_merge($args, $options);
    }
    $this->post('cards', $args);
  }

  /**
   * @param $hash - avatar hash for gravatar.
   * @param string $size -
   * @return string - link to image.
   */
  private function getAvatarFromGravatar($hash, $size = 'small') {
    return url("https://gravatar.com/avatar/$hash", array('external' => TRUE));
  }

  /**
   * @param string $hash - avatar hash for trello.
   * @param string $size - 'small' for 30x30 px or 'medium' for 50x50 px.
   * @return string - link to image.
   */
  private function getAvatarFromTrello($hash, $size = 'small') {
    switch ($size) {
      case 'medium':
        $size = 50;
        break;
      case 'small':
      default:
        $size = 30;
        break;
    }

    return url("https://trello-avatars.s3.amazonaws.com/$hash/$size.png", array('external' => TRUE));
  }


  /**
   * @param object $member
   * @return bool|string - FALSE or link to image.
   */
  private function getAvatar($member) {
    switch ($member->avatarSource) {
      case 'upload':
        $avatar = $this->getAvatarFromTrello($member->avatarHash);
        break;

      case 'gravatar':
        $avatar = $this->getAvatarFromGravatar($member->gravatarHash);
        break;

      case 'none':
      default:
        $avatar = FALSE;
        break;
    }

    return $avatar;
  }

  /**
   * @param $idCard - id of card.
   * @return array|bool - false if card is not exist or array if card exist.
   */
  public function getCard($idCard) {
    $this->get("cards/$idCard");
    if (!empty($this->result['data'][0])) {
      $result = $this->result['data'][0];

      $labels = array();
      if (!empty($result->labels)) {
        foreach ($result->labels as $label) {
          $labels[] = $label->color;
        }
      }

      $members = array();

      if (!empty($result->idMembers)) {
        foreach ($result->idMembers as $idMember) {
          $this->get("members/$idMember");
          $member = !empty($this->result['data'][0]) ? $this->result['data'][0] : FALSE;
          if ($member) {
            $members[] = array(
              'name' => !empty($member->fullName) ? $member->fullName : FALSE,
              'url' => $member->url,
              'avatar' => $this->getAvatar($member),
            );
          }
        }
      }

      if (!empty($result->idBoard)) {
        $idBoard = $result->idBoard;
        $this->get("boards/$idBoard");
        $board['name'] = $this->result['data'][0]->name;
        $board['url'] = $this->result['data'][0]->url;
      }

      return array(
        'name' => !empty($result->name) ? $result->name : FALSE,
        'due' => $result->due,
        'url' => $result->url,
        'labels' => !empty($labels) ? $labels : FALSE,
        'comments' => !empty($result->badges->comments) ? $result->badges->comments : 0,
        'check_list' => !empty($result->badges->checkItems) ? ($result->badges->checkItemsChecked . '/' . $result->badges->checkItems) : FALSE,
        'members' => !empty($members) ? $members : FALSE,
        'board' => !empty($board) ? $board : FALSE,
      );
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get all boards.
   */
  public function getBoards() {
    $options = array('filter' => 'open');
    $this->get('/member/me/boards', $options);
    $items = array();
    if (!empty($this->result['data'][0])) {
      $boards = $this->result['data'];
      foreach ($boards as $board) {
        $items[$board->id] = $board->name;
      }
    }

    return $items;
  }

  /**
   * Get lists on board.
   * @return array id => name.
   */
  public function getLists($idBoard) {
    $this->get("/boards/$idBoard/lists");
    $items = array();
    if (!empty($this->result['data'][0])) {
      $lists = $this->result['data'];
      foreach ($lists as $list) {
        $items[$list->id] = $list->name;
      }
    }

    return $items;
  }

  /**
   * Create webhook.
   */
  public function createWebhook($idModel, $description = '', $callbackURL = TRELLO_CALLBACK_URL) {
    $params = array(
      'description' => $description,
      'callbackURL' => $GLOBALS['base_url'] . '/' . $callbackURL,
      'idModel' => $idModel,
    );

    $this->post('webhooks', $params);
  }
}
