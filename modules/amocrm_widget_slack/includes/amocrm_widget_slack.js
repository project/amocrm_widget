(function ($) {

  Drupal.behaviors.amocrmWidgetSlack = {
    attach: function (context, settings) {
      $('#post_message').once('slack-post-message-button', function (){
        $(this).on('click', function(element) {
          initPreloader();
          var options = {
            url: Drupal.settings.basePath + 'amocrm-widget-slack/post-message',
            submit: {

            }
          };
          var ajax = new Drupal.ajax(false, false, options);
          ajax.eventResponse(ajax, {});

          element.preventDefault();
        });
      });
      $('#create_channel').once('slack-create-channel-button', function(){
        $(this).on('click', function(element) {
          var options = {
            url: Drupal.settings.basePath + 'amocrm-widget-slack/create-channel',
            submit: {

            }
          };
          var ajax = new Drupal.ajax(false, false, options);
          ajax.eventResponse(ajax, {});

          element.preventDefault();
        });
      });
      $('.archive_channel').once('slack-archive-file-link', function(){
        $(this).on('click', function(element) {
          initPreloader();

          var id = $(this).data('channelId');

          var options = {
            url: Drupal.settings.basePath + 'amocrm-widget-slack/archive-channel',
            submit: {
              channel_id: id
            }
          };
          var ajax = new Drupal.ajax(false, false, options);
          ajax.eventResponse(ajax, {});

          element.preventDefault();
        });
      });
    }
  };

})(jQuery);
