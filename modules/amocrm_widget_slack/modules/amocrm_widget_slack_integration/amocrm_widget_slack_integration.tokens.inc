<?php

/**
 * Implements hook_info().
 */
function amocrm_widget_slack_integration_token_info() {
  $default_type = 'amocrm_widget_slack_integration_default';
  $channel_type = 'amocrm_widget_slack_integration_created_channel';

  $types[$default_type] = array(
    'name' => t('Slack default'),
    'description' => t("Default tokens."),
  );
  $types[$channel_type] = array(
    'name' => t('Created channel'),
    'description' => t("Created channel tokens."),
  );

  $default['slack-channel'] = array(
    'name' => t('Channel'),
    'description' => t('Name of channel.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );
  $default['slack-default-webhook-url'] = array(
    'name' => t('Webhook URL'),
    'description' => t('Webhook URL which defined on settings page.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );
  $default['slack-icon-url'] = array(
    'name' => t('Icon URL'),
    'description' => t('Absolute Url to image what you would like to use for your Slackbot.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );
  $default['slack-message'] = array(
    'name' => t('Message'),
    'description' => t('User message'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );
  $default['slack-icon-emoji'] = array(
    'name' => t('Emoji code'),
    'description' => t('Emoji code what you would like to use for your Slackbot.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );
  $default['slack-username'] = array(
    'name' => t('Username'),
    'description' => t('Default username for your Slackbot.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );

  $default['slack-attachment-enabled'] = array(
    'name' => t('Enable message attachments'),
    'description' => t('Allows using further styling options for messages.'),
    'type' => 'boolean',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );

  $default['slack-attachment-color'] = array(
    'name' => t('Attachment color'),
    'description' => t('Default color for your Slackbot messages.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );

  $default['slack-attachment-title'] = array(
    'name' => t('Attachment title'),
    'description' => t('Set a title for your messages.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );

  $default['slack-attachment-title-link'] = array(
    'name' => t('Attachment title link'),
    'description' => t('Link your message tittle to a url.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );

  $default['slack-attachment-pretext'] = array(
    'name' => t('Attachment pretext'),
    'description' => t('Add some meta information precding your actual message.'),
    'type' => 'text',
    'dynamic' => TRUE,
    'global_types' => TRUE,
  );

  $channel['id'] = array(
    'name' => t('ID'),
    'description' => t('Channel id'),
  );
  $channel['name'] = array(
    'name' => t('Name'),
    'description' => t('Channel name'),
  );
  $channel['type'] = array(
    'name' => t('Type'),
    'description' => t('Channel type name'),
  );
  $channel['source'] = array(
    'name' => t('Source'),
    'description' => t('Channel source name'),
  );

  return array(
    'types' => $types,
    'tokens' => array(
      $default_type => $default,
      $channel_type => $channel,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function amocrm_widget_slack_integration_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if (strpos($type, 'amocrm_widget_slack_integration_') === FALSE) {
    return $replacements;
  }
  $data = $data[$type];

  foreach ($tokens as $name => $original) {
    if ($name == 'slack-icon-url') {
      $replacements[$original] = 'image';
    }
    if ($name == 'slack-icon-emoji') {
      $replacements[$original] = 'emoji';
    }
    if ($name == 'slack-default-webhook-url') {
      $replacements[$original] = amocrm_widget_slack_integration_get_default_webhook_url();
    }
    if ($name == 'slack-username') {
      $replacements[$original] = amocrm_widget_slack_integration_get_default_username();
    }

    switch ($name) {
      case 'id':
      case 'name':
        $replacements[$original] = $data[$name];
        break;

      case 'type':
        $replacements[$original] = SLACK_CHANNEL_TYPE;
        break;

      case 'source':
        $replacements[$original] = 'slack';
        break;
    }
  }

  return $replacements;
}
