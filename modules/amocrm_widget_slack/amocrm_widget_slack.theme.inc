<?php

/**
 * Prepare data for widget template.
 */
function template_preprocess_amocrm_widget_slack_widget(&$vars) {
  if (!empty($vars['options']['create_channel'])) {
    $vars['create_channel'] = $vars['options']['create_channel'];
  }
  if (!empty($vars['options']['urls'])) {
    $vars['urls'] = $vars['options']['urls'];
  }
  if (!empty($vars['options']['chatlist'])) {
    foreach ($vars['options']['chatlist'] as $chat) {
      $links = array();
      if (!$chat['is_archived']) {
        $channel_url = trim(variable_get('amocrm_widget_slack_integration_chat_url', '')) . '/' . $chat['name'];

        if (drupal_strlen($chat['name']) >= 20) {
          $chat['name'] = drupal_substr($chat['name'], 0, 19)."...";
        }

        $links[] = l('#' . $chat['name'], $channel_url, array(
          'attributes' => array('target' => '_blank'),
          'external' => TRUE,
        ));

        $links[] = l('x', '#', array(
          'attributes' => array(
            'class' => 'archive_channel',
            'style' => 'color: #E7392D',
            'data-channel-id' => $chat['id'],
          ),
        ));

        $vars['chatlist'][] = implode(' ', $links);
      }
    }
  }
}
