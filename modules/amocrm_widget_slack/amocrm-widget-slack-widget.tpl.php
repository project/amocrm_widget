<?php
/**
 * @file
 * Template for slack widget.
 */
?>

<div class="widget-slack">
  <?php if (!empty($chatlist)): ?>
    <div class="widget-slack-list">
      <div class="widget-slack-list-title">
        <?php print t('channels'); ?>
      </div>
      <div class="widget-slack-list-channels">
        <ul>
          <?php foreach ($chatlist as $chat): ?>
            <li><?php print $chat; ?></li>
          <?php endforeach ?>
        </ul>
      </div>
    </div>

    <button id="post_message" class="btn btn-default btn-slack-widget">
      <i class="ico-write"></i>
      <?php print t('Post message'); ?>
    </button>
  <?php endif ?>

  <button id="create_channel" class="btn btn-default btn-slack-widget">
    <i class="ico-write"></i>
    <?php print t('Create channel'); ?>
  </button>
</div>
