<?php
/**
 * @file
 * Forms for module amoCRM import from 2gis.
 */

/**
 * Form for cart.
 */
function dgis_amocrm_import_cart_form($form, $form_state, $show_button = TRUE) {
  $form['#attributes'] = array('class' => array('dgis-amocrm-cart'));
  $form['#action'] = '/admin/config/services/2gis/amocrm-import/send';
  $form['dgis_all_items'] = array(
    '#type' => 'markup',
    '#markup' => t('Send to amoCRM: ') . (isset($_SESSION['dgis_amocrm_cart']) ? count($_SESSION['dgis_amocrm_cart']) : 0),
    '#prefix' => '<div class="clearfix"><b>',
    '#suffix' => '</b></div>',
  );
  if ($show_button) {
    $form['send_to_amocrm'] = array(
      '#type' => 'submit',
      '#value' => t('Send'),
    );
  }

  return $form;
}

/**
 * Form for each views line to include/exclude from the cart.
 */
function dgis_amocrm_import_cart_exclude_form($form, $form_state, $dgis_firm_data) {
  $reference = dgis_amocrm_import_get_reference($dgis_firm_data->id);

  $form['#attributes'] = array('class' => array('dgis-exclude-' . $dgis_firm_data->id));
  $form['dgis_exclude_from_cart_id'] = array(
    '#type' => 'hidden',
    '#value' => $dgis_firm_data->id,
  );
  $form['dgis_exclude_from_cart_hash'] = array(
    '#type' => 'hidden',
    '#value' => $dgis_firm_data->dgis_catalog_hash,
  );
  if (empty($reference)) {
    if (isset($_SESSION['dgis_amocrm_cart'][$dgis_firm_data->id])) {
      $form['dgis_exclude_from_cart'] = array(
        '#type' => 'submit',
        '#value' => t("Don't send"),
        '#ajax' => array(
          'callback' => 'dgis_amocrm_import_cart_exclude_form_submit_exclude',
          'event' => 'click',
        ),
      );
    } else {
      $form['dgis_exclude_from_cart'] = array(
        '#type' => 'submit',
        '#value' => t('Send to amoCRM'),
        '#ajax' => array(
          'callback' => 'dgis_amocrm_import_cart_exclude_form_submit_include',
          'event' => 'click',
        ),
      );
    }
  }
  else {
    $form['dgis_already_imported'] = array(
      '#type' => 'submit',
      '#value' => t('Imported'),
      '#disabled' => TRUE,
    );
  }

  return $form;
}

/**
 * Ajax submit for dgis_amocrm_import_cart_exclude_form.
 */
function dgis_amocrm_import_cart_exclude_form_submit_include($form, $form_state) {
  $id = $form_state['values']['dgis_exclude_from_cart_id'];
  $hash = $form_state['values']['dgis_exclude_from_cart_hash'];
  $_SESSION['dgis_amocrm_cart'][$id] = $hash;
  $form = drupal_rebuild_form($form['#form_id'], $form_state);
  $cart = drupal_get_form('dgis_amocrm_import_cart_form');
  $commands = array();
  $commands[] = ajax_command_replace('.dgis-exclude-' . $id, render($form));
  $commands[] = ajax_command_replace('.dgis-amocrm-cart', render($cart));

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Ajax submit for dgis_amocrm_import_cart_exclude_form.
 */
function dgis_amocrm_import_cart_exclude_form_submit_exclude($form, $form_state) {
  $id = $form_state['values']['dgis_exclude_from_cart_id'];
  unset($_SESSION['dgis_amocrm_cart'][$id]);
  $form = drupal_rebuild_form($form['#form_id'], $form_state);
  $cart = drupal_get_form('dgis_amocrm_import_cart_form');
  $commands = array();
  $commands[] = ajax_command_replace('.dgis-exclude-' . $id, render($form));
  $commands[] = ajax_command_replace('.dgis-amocrm-cart', render($cart));

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Dgis firm send form.
 */
function dgis_amocrm_import_send_form($form, $form_state) {
  $form['dgis_amocrm_create_contact'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create contact'),
  );
  $form['dgis_amocrm_create_task'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create task'),
    '#states' => array(
      'visible' => array(
        ':input[name="dgis_amocrm_create_contact"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['dgis_amocrm_tasks_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Template for task title'),
    '#default_value' => t('Make a call to "@name"'),
    '#description' => t('You can use tokens: @name (company name), @city_name (City), @address (Address), @rubrics (Rubrics)'),
    '#states' => array(
      'visible' => array(
        ':input[name="dgis_amocrm_create_task"]' => array('checked' => TRUE),
        ':input[name="dgis_amocrm_create_contact"]' => array('checked' => TRUE),
      ),
    ),
  );
  // Get options for dgis_amocrm_assignee.
  $options = _dgis_amocrm_import_assignee_load();
  $form['dgis_amocrm_assignee'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => $options,
    '#title' => t('Assignee'),
    '#states' => array(
      'visible' => array(
        ':input[name="dgis_amocrm_create_task"]' => array('checked' => TRUE),
        ':input[name="dgis_amocrm_create_contact"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['dgis_amocrm_starting_from'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'm/d/Y',
    '#datepicker_options' => array(
      'minDate' => '0',
    ),
    '#title' => t('Starting from'),
    '#states' => array(
      'visible' => array(
        ':input[name="dgis_amocrm_create_task"]' => array('checked' => TRUE),
        ':input[name="dgis_amocrm_create_contact"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['dgis_amocrm_work_days'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Working days'),
    '#options' => array(
      0 => t('Monday'),
      1 => t('Tuesday'),
      2 => t('Wednesday'),
      3 => t('Thursday'),
      4 => t('Friday'),
      5 => t('Saturday'),
      6 => t('Sunday'),
    ),
    '#default_value' => array(0,1,2,3,4),
    '#states' => array(
      'visible' => array(
        ':input[name="dgis_amocrm_create_task"]' => array('checked' => TRUE),
        ':input[name="dgis_amocrm_create_contact"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['dgis_amocrm_tasks_per_day_num'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of tasks per day'),
    '#states' => array(
      'visible' => array(
        ':input[name="dgis_amocrm_create_task"]' => array('checked' => TRUE),
        ':input[name="dgis_amocrm_create_contact"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['dgis_amocrm_create_company'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create company'),
  );

  $form['send_to_amocrm'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Dgis firm send form validate.
 */
function dgis_amocrm_import_send_form_validate($form, $form_state) {
  if ($form_state['values']['dgis_amocrm_create_task'] == 1) {
    if (empty($form_state['values']['dgis_amocrm_tasks_title'])) {
      form_set_error('dgis_amocrm_tasks_title', t('Task title cannot be empty'));
    }
    if (empty($form_state['values']['dgis_amocrm_assignee'])) {
      form_set_error('dgis_amocrm_assignee', t('Select user for tasks'));
    }
    if (empty($form_state['values']['dgis_amocrm_starting_from'])) {
      form_set_error('dgis_amocrm_starting_from', t('Date cannot be empty'));
    }
    else {
      $today = date('Y-m-d');
      if ($today > $form_state['values']['dgis_amocrm_starting_from']) {
        form_set_error('dgis_amocrm_starting_from', t('You can not create a tasks in the past.'));
      }
    }
    if (!empty($form_state['values']['dgis_amocrm_tasks_per_day_num'])) {
      if (!is_numeric($form_state['values']['dgis_amocrm_tasks_per_day_num'])) {
        form_set_error('dgis_amocrm_tasks_per_day_num', t('Number of tasks per day should be numeric'));
      }
    }
  }
}

/**
 * Load assignee from amoCRM.
 */
function _dgis_amocrm_import_assignee_load() {
  $client = amocrm_api_get_default_client();
  $assignee = amocrm_api_users_load($client);

  return $assignee;
}

/**
 * Batch submit for dgis_amocrm_import_send_form.
 */
function dgis_amocrm_import_send_form_submit($form, &$form_state) {
  $operations[] = array('dgis_amocrm_import_save_firm', array($form_state));
  $batch = array(
    'operations' => $operations,
    'finished' => 'dgis_amocrm_import_save_firm_finished',
    'title' => t('Saving firms'),
    'progress_message' => t('Complete @current of @total.'),
    'error_message' => t('Error.'),
  );
  batch_set($batch);
}

/**
 * Batch operation saving firm.
 */
function dgis_amocrm_import_save_firm($form_state, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['max'] = count($_SESSION['dgis_amocrm_cart']);
    $context['results']['errors'] = array(
      'imported' => array(),
      'company' => array(),
      'contact' => array(),
      'task' => array(),
    );

    $context['sandbox']['work_day_names'] = dgis_amocrm_import_get_work_day_names($form_state['values']['dgis_amocrm_work_days']);
    $context['sandbox']['date'] = dgis_amocrm_import_get_date($form_state['values']['dgis_amocrm_starting_from'], $context['sandbox']['work_day_names']);
    $context['sandbox']['client'] = amocrm_api_get_default_client();

    $context['sandbox']['contact_mapping'] = amocrm_widget_dgis_get_corresponding_contact_codes($context['sandbox']['client'], AMOCRM_TYPE_CONTACT);
    $context['sandbox']['company_mapping'] = amocrm_widget_dgis_get_corresponding_contact_codes($context['sandbox']['client'], AMOCRM_TYPE_COMPANY);

    $context['sandbox']['users'] = array_keys($form_state['values']['dgis_amocrm_assignee']);
    $context['sandbox']['tasks_per_day'] = count($context['sandbox']['users']) * $form_state['values']['dgis_amocrm_tasks_per_day_num'];
    $context['sandbox']['progress'] = 0;
  }
  $current = each($_SESSION['dgis_amocrm_cart']);
  $id = $current['key'];
  $hash = $current['value'];
  $references = dgis_amocrm_import_get_reference($id);
  if (!empty($references)) {
    unset($_SESSION['dgis_amocrm_cart'][$id]);
    $context['sandbox']['progress'] += 1;
    if ($context['sandbox']['progress'] <= $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
    $context['results']['errors']['imported'][] = $id;

    return TRUE;
  }

  $firm = dgis_get_firm_profile($id, $hash);

  // Create company.
  $company_name = '';
  if (!empty($form_state['values']['dgis_amocrm_create_company'])) {
    $amocrm_company = array('name' => $firm['name']);
    $company_info = $firm['contacts'][0];
    $cached_info = '';
    if (!empty($context['sandbox']['company_mapping'])) {
      foreach ($company_info['contacts'] as $field) {
        $custom_field = array();
        if (array_key_exists($field['type'], $context['sandbox']['company_mapping'])) {
          $custom_field['id'] = $context['sandbox']['company_mapping'][$field['type']]['id'];
          $value = array();
          $value['value'] = !empty($field['alias'])
            ? $field['alias']
            : $field['value'];
          if (!empty($context['sandbox']['company_mapping'][$field['type']]['enum'])) {
            $value['enum'] = $context['sandbox']['company_mapping'][$field['type']]['enum'];
          }
          $custom_field['values'][] = $value;
          $amocrm_company['custom_fields'][] = $custom_field;
          $cached_info = serialize(array(
            'id' => $id,
            'hash' => $hash,
            'data' => $firm,
            ));
        }
      }
    }
    $amocrm_company['custom_fields'][] = array(
      'id' => variable_get('dgis_amocrm_widget_linking_field_id'),
      'values' => array(array('value' => $cached_info)),
    );
    $amocrm_company['custom_fields'][] = array(
      'id' => $context['sandbox']['company_mapping']['address']['id'],
      'values' => array(array('value' => $firm['city_name'] . ', ' . $firm['address'])),
    );
    $company = amocrm_api_companies_add($context['sandbox']['client'], array($amocrm_company));
    if (empty($company['contacts']['add'][0])) {
      $context['sandbox']['progress'] += 1;
      if ($context['sandbox']['progress'] <= $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
      $context['results']['errors']['company'][] = $id;

      return TRUE;
    }
    else {
      $company_name = $firm['name'];
      dgis_amocrm_import_set_reference($id, $company['contacts']['add'][0]['id']);
    }
  }

  // Create contacts.
  if (!empty($form_state['values']['dgis_amocrm_create_contact'])) {
    $contacts = array();
    foreach ($firm['contacts'] as $contact) {
      $amocrm_contact = array(
        'name' => !empty($contact['name']) ? $firm['name'] . ': ' . $contact['name'] : $firm['name'],
      );
      if (!empty($company_name)) {
        $amocrm_contact['company_name'] = $company_name;
      }
      if (!empty($context['sandbox']['contact_mapping'])) {
        foreach ($contact['contacts'] as $field) {
          $custom_field = array();
          if (array_key_exists($field['type'], $context['sandbox']['contact_mapping'])) {
            $custom_field['id'] = $context['sandbox']['contact_mapping'][$field['type']]['id'];
            $value = array();
            $value['value'] = !empty($field['alias'])
              ? $field['alias']
              : $field['value'];
            if (!empty($context['sandbox']['contact_mapping'][$field['type']]['enum'])) {
              $value['enum'] = $context['sandbox']['contact_mapping'][$field['type']]['enum'];
            }
            $custom_field['values'][] = $value;
            $amocrm_contact['custom_fields'][] = $custom_field;
          }
        }
      }
      if (!empty($amocrm_contact['custom_fields'])) {
        $contacts[] = $amocrm_contact;
      }
    }
    if (empty($company_name)) {
      $contacts[0]['name'] = $firm['name'];
    }
    $result = amocrm_api_contacts_add($context['sandbox']['client'], $contacts);
    if (!empty($result[AMOCRM_TYPE_CONTACT]['add'][0]['id'])) {
      $contact_id = $result[AMOCRM_TYPE_CONTACT]['add'][0]['id'];
    }
    else {
      $context['sandbox']['progress'] += 1;
      if ($context['sandbox']['progress'] <= $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
      $context['results']['errors']['contact'][] = $id;

      return TRUE;
    }
  }

  // Create task.
  if (!empty($form_state['values']['dgis_amocrm_create_task']) && !empty($contact_id)) {
    $user_index = $context['sandbox']['progress'] % count($context['sandbox']['users']);
    $user = $context['sandbox']['users'][$user_index];
    $replacements = array(
      '@name' => $firm['name'],
      '@city_name' => $firm['city_name'],
      '@address' => $firm['address'] ? $firm['address'] : '',
      '@rubrics' => implode(', ', $firm['rubrics']),
    );
    $task = array(
      'element_id' => $contact_id,
      'element_type' => amocrm_api_get_code_by_string(AMOCRM_TYPE_CONTACT),
      'task_type' => 1,
      'text' => format_string($form_state['values']['dgis_amocrm_tasks_title'], $replacements),
      'responsible_user_id' => $user,
      'complete_till' => $context['sandbox']['date'],
    );
    $result = amocrm_api_tasks_add($context['sandbox']['client'], array($task));
    if (empty($result['tasks']['add'][0]['id'])) {
      $context['sandbox']['progress'] += 1;
      if ($context['sandbox']['progress'] <= $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
      $context['results']['errors']['task'][] = $id;

      return TRUE;
    }

    if (!empty($context['sandbox']['tasks_per_day'])) {
      if ((($context['sandbox']['progress'] + 1) % $context['sandbox']['tasks_per_day']) === 0) {
        $context['sandbox']['date'] = dgis_amocrm_import_get_next_date($context['sandbox']['date'], $context['sandbox']['work_day_names']);
      }
    }
  }

  $context['results']['success'][] = $id;
  $context['sandbox']['progress'] += 1;
  if ($context['sandbox']['progress'] <= $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

  unset($_SESSION['dgis_amocrm_cart'][$id]);

  return TRUE;
}

/**
 * Batch finish.
 */
function dgis_amocrm_import_save_firm_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(count($results['success']) . ' firm(s) sent to amoCRM.');
    if (!empty($results['errors']['imported'])) {
      drupal_set_message(count($results['errors']['imported']) . ' firm(s) already sent.', 'warning');
    }
    if (!empty($results['errors']['company'])) {
      drupal_set_message(count($results['errors']['company']) . " firm(s) can't create company.", 'warning');
    }
    if (!empty($results['errors']['contact'])) {
      drupal_set_message(count($results['errors']['contact']) . " firm(s) can't create contact.", 'warning');
    }
    if (!empty($results['errors']['task'])) {
      drupal_set_message(count($results['errors']['task']) . " firm(s) can't create task.", 'warning');
    }
  }
  else {
    drupal_set_message('Error', 'error');
  }
}

/**
 * Get timestamp for creating task.
 */
function dgis_amocrm_import_get_date($date, $work_days = '') {
  $date = $date . ' 23:59';
  $timestamp = strtotime($date);
  $week_day = date('l', $timestamp);
  if (in_array($week_day, $work_days) || empty($work_days)) {
    return $timestamp;
  }

  return dgis_amocrm_import_get_next_date($timestamp, $work_days);
}

/**
 * Get next date for creating task.
 */
function dgis_amocrm_import_get_next_date($timestamp, $work_day_names = '') {
  static $work_days;
  if (!empty($work_day_names)) {
    $work_days = $work_day_names;
  }
  $timestamp += 86400;
  $week_day = date('l', $timestamp);
  if (in_array($week_day, $work_days)) {

    return $timestamp;
  }

  return dgis_amocrm_import_get_next_date($timestamp);
}

/**
 * Get work day names.
 */
function dgis_amocrm_import_get_work_day_names($work_days) {
  $days = array(
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  );
  $work_day_names = array();
  foreach ($work_days as $k => $val) {
    if ($val !== 0) {
      $work_day_names[] = $days[$k];
    }
  }

  return !empty($work_day_names) ? $work_day_names : $days;
}
