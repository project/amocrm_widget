<?php
/**
 * @file
 * dgis_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function dgis_amocrm_import_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'dgis_amocrm_import_view';
  $view->description = 'Views for the amoCRM import from 2gis';
  $view->tag = 'default';
  $view->base_table = 'dgis_catalog';
  $view->human_name = '2gis catalog view for amoCRM';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Import contacts from 2gis into the amoCRM';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'city_name' => 'city_name',
    'rubrics' => 'rubrics',
    'name' => 'name',
    'address' => 'address',
    'micro_comment' => 'micro_comment',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'city_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'rubrics' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'address' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'micro_comment' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: 2gis catalog query: City name */
  $handler->display->display_options['fields']['city_name']['id'] = 'city_name';
  $handler->display->display_options['fields']['city_name']['table'] = 'dgis_catalog';
  $handler->display->display_options['fields']['city_name']['field'] = 'city_name';
  /* Field: 2gis catalog query: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'dgis_catalog';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: 2gis catalog query: Address */
  $handler->display->display_options['fields']['address']['id'] = 'address';
  $handler->display->display_options['fields']['address']['table'] = 'dgis_catalog';
  $handler->display->display_options['fields']['address']['field'] = 'address';
  /* Field: 2gis catalog query: Rubrics */
  $handler->display->display_options['fields']['rubrics']['id'] = 'rubrics';
  $handler->display->display_options['fields']['rubrics']['table'] = 'dgis_catalog';
  $handler->display->display_options['fields']['rubrics']['field'] = 'rubrics';
  /* Field: 2gis catalog query: Description */
  $handler->display->display_options['fields']['micro_comment']['id'] = 'micro_comment';
  $handler->display->display_options['fields']['micro_comment']['table'] = 'dgis_catalog';
  $handler->display->display_options['fields']['micro_comment']['field'] = 'micro_comment';
  /* Field: 2gis catalog query: Hash */
  $handler->display->display_options['fields']['hash']['id'] = 'hash';
  $handler->display->display_options['fields']['hash']['table'] = 'dgis_catalog';
  $handler->display->display_options['fields']['hash']['field'] = 'hash';
  $handler->display->display_options['fields']['hash']['label'] = '';
  $handler->display->display_options['fields']['hash']['exclude'] = TRUE;
  $handler->display->display_options['fields']['hash']['element_label_colon'] = FALSE;
  /* Field: 2gis catalog query: ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'dgis_catalog';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  /* Field: 2gis catalog query: Send to cart */
  $handler->display->display_options['fields']['dgis_exclude_from_cart']['id'] = 'dgis_exclude_from_cart';
  $handler->display->display_options['fields']['dgis_exclude_from_cart']['table'] = 'dgis_catalog';
  $handler->display->display_options['fields']['dgis_exclude_from_cart']['field'] = 'dgis_exclude_from_cart';
  $handler->display->display_options['fields']['dgis_exclude_from_cart']['label'] = 'Action';
  $handler->display->display_options['filter_groups']['operator'] = 'OR';
  /* Filter criterion: 2gis catalog query: What */
  $handler->display->display_options['filters']['dgis_catalog_what']['id'] = 'dgis_catalog_what';
  $handler->display->display_options['filters']['dgis_catalog_what']['table'] = 'dgis_catalog';
  $handler->display->display_options['filters']['dgis_catalog_what']['field'] = 'dgis_catalog_what';
  $handler->display->display_options['filters']['dgis_catalog_what']['exposed'] = TRUE;
  $handler->display->display_options['filters']['dgis_catalog_what']['expose']['operator_id'] = 'dgis_catalog_what_op';
  $handler->display->display_options['filters']['dgis_catalog_what']['expose']['label'] = 'What';
  $handler->display->display_options['filters']['dgis_catalog_what']['expose']['operator'] = 'dgis_catalog_what_op';
  $handler->display->display_options['filters']['dgis_catalog_what']['expose']['identifier'] = 'dgis_catalog_what';
  $handler->display->display_options['filters']['dgis_catalog_what']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: 2gis catalog query: Where */
  $handler->display->display_options['filters']['dgis_catalog_where']['id'] = 'dgis_catalog_where';
  $handler->display->display_options['filters']['dgis_catalog_where']['table'] = 'dgis_catalog';
  $handler->display->display_options['filters']['dgis_catalog_where']['field'] = 'dgis_catalog_where';
  $handler->display->display_options['filters']['dgis_catalog_where']['exposed'] = TRUE;
  $handler->display->display_options['filters']['dgis_catalog_where']['expose']['operator_id'] = 'dgis_catalog_where_op';
  $handler->display->display_options['filters']['dgis_catalog_where']['expose']['label'] = 'Where';
  $handler->display->display_options['filters']['dgis_catalog_where']['expose']['operator'] = 'dgis_catalog_where_op';
  $handler->display->display_options['filters']['dgis_catalog_where']['expose']['identifier'] = 'dgis_catalog_where';
  $handler->display->display_options['filters']['dgis_catalog_where']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $export['dgis_amocrm_import_view'] = $view;

  return $export;
}
