<?php

/**
 * @file
 * LDAP field handler
 *
 * Defines a new class field handler for a default ldap field
 */
class dgis_amocrm_import_views_handler_field extends views_handler_field {

  function render($values) {
    $form = drupal_get_form('dgis_amocrm_import_cart_exclude_form', $values);
    return render($form);
  }
}
