var widgetEnv = {
    amoCrmDrupal: new amoCrmDrupal(),
    data: {}
};

var initPreloader = function() {
  jQuery('body').addClass('block-processing');
  jQuery('<div class="block-processing__wrap"></div>').insertAfter('.panel-amo-widget-group');
};

widgetEnv.amoCrmDrupal.init(window.parent);

(function ($, widgetEnv) {
    Drupal.ajax.prototype.commands.amocrmWidgetOpenForm = function(ajax, data, status) {
        var form = {
            message: data.form.message,
            buttonConfirm: data.form.button_confirm,
            buttonCancel: data.form.button_cancel,
            fields: data.form.fields
        };

        widgetEnv.amoCrmDrupal.call('modalForm', form, function (formData) {

            if (typeof formData === 'object') {
                formData.callback = data.form.callback;
                formData.context = widgetEnv.data.card;
                var options = {
                    url: Drupal.settings.basePath + 'amocrm_widget/form-submit',
                    submit: formData
                };
                var ajax = new Drupal.ajax(false, false, options);
                ajax.eventResponse(ajax, {});
            }
        });
    };

    Drupal.ajax.prototype.commands.amocrmWidgetReload = function (ajax, data, status) {
        widgetEnv.amoCrmDrupal.call('reload');
    };

    Drupal.ajax.prototype.commands.amocrmWidgetResize = function (ajax, data, status) {
        widgetEnv.amoCrmDrupal.call('windowHeight', $('html').height());
    };

    Drupal.ajax.prototype.commands.amocrmWidgetHome = function (ajax, data, status) {
        widgetEnv.amoCrmDrupal.call('windowHeight', $('html').height());
    };

    Drupal.ajax.prototype.commands.amocrmWidgetRemovePreloader = function (ajax, data, status) {
        $('body').removeClass('block-processing');
        $('div.block-processing__wrap').remove();
    };

    Drupal.ajax.prototype.commands.amocrmWidgetMessagePopUp = function(ajax, data, status) {

        var message = {
            message: data.form.message,
            buttonConfirm: data.form.button_confirm
            //buttonCancel: data.form.button_cancel
        };


        widgetEnv.amoCrmDrupal.call('modalConfirm', message, function () {
            var formData = {
                callback: data.form.callback,
                context: widgetEnv.data.card
            };
            var options = {
                url: Drupal.settings.basePath + 'amocrm_widget/form-submit',
                submit: formData
            };
            var ajax = new Drupal.ajax(false, false, options);
            ajax.eventResponse(ajax, {});
        });
    };

    Drupal.behaviors.amocrmWidgetHomeLink = {
        attach: function (context, settings) {
            $('.panel-amo-widget-close').on('click', function(element) {
                element.preventDefault();

                var options = {
                    url: Drupal.settings.basePath + 'amocrm_widget/widget-page',
                    submit: {
                        page_type: Drupal.settings.amoCRMWidget.pageType,
                        context: Drupal.settings.amoCRMWidget.context
                    }
                };
                var ajax = new Drupal.ajax(false, false, options);
                ajax.eventResponse(ajax, {});
            });
        }
    };
})(jQuery, widgetEnv);
