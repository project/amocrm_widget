<?php
/**
 * @file
 * Theme function of Be sure module.
 */

/**
 * Process variables for amocrm-widget-page.tpl.php.
 */
function template_preprocess_amocrm_widget_page(&$vars) {
  $vars['title'] = drupal_get_title();
}

/**
 * Process variables for amocrm-widget-html.tpl.php.
 */
function template_preprocess_amocrm_widget_html(&$vars) {

  $widget_module_path = drupal_get_path('module', 'amocrm_widget');

  drupal_add_library('system', 'drupal.ajax');
  drupal_add_css($widget_module_path . '/includes/widget_fonts.css');
  drupal_add_css($widget_module_path . '/includes/widget_style.css');
  drupal_add_js($widget_module_path . '/includes/client.js');
  drupal_add_js($widget_module_path . '/includes/amoCrmDrupal.js');
  drupal_add_js($widget_module_path . '/includes/amocrm_widget.js');
  drupal_add_js($widget_module_path . '/includes/amocrm_widget_preloader.js');

  // @todo Move it into submodules.
  $amocrm_widget_trello = drupal_get_path('module', 'amocrm_widget_trello');
  drupal_add_js($amocrm_widget_trello . '/includes/amocrm_widget_trello.js');
  drupal_add_css($amocrm_widget_trello . '/includes/widget-trello.css');

  $amocrm_widget_dgis = drupal_get_path('module', 'amocrm_widget_dgis');
  drupal_add_css($amocrm_widget_dgis . '/css/widget-2gis-fonts.css');
  drupal_add_css($amocrm_widget_dgis . '/css/widget-2gis-style.css');
  drupal_add_css($amocrm_widget_dgis . '/css/widget-2gis.css');

  $amocrm_widget_mail = drupal_get_path('module', 'amocrm_widget_mail');
  drupal_add_js($amocrm_widget_mail . '/includes/amocrm_widget_mail.js');
  drupal_add_css($amocrm_widget_mail . '/includes/widget-mail.css');

  $amocrm_widget_slack = drupal_get_path('module', 'amocrm_widget_slack');
  drupal_add_js($amocrm_widget_slack . '/includes/amocrm_widget_slack.js');
  drupal_add_css($amocrm_widget_slack . '/includes/widget-slack.css');

  $vars['js'] = drupal_get_js();
  $vars['css'] = drupal_get_css();
}

/**
 * Process variables for amocrm-widget-list.tpl.php.
 */
function template_preprocess_amocrm_widget_preload(&$vars) {
  $preloader_path = drupal_get_path('module', 'amocrm_widget') . '/includes/amocrm_widget_preloader.gif';
  $vars['preloader'] = theme('image', array('path' => $preloader_path));
}
