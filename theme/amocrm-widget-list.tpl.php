<?php

/**
 * @file
 * Template for widget list.
 */
?>

<?php foreach($elements as $group_name => $widget): ?>
  <div class="panel-amo-widget">
    <div class="panel-amo-widget-head">
      <?php print render($group_name); ?>
      <div class="panel-amo-widget-caret"></div>
    </div>
    <div class="panel-amo-widget-body">
      <?php foreach($widget as $link_info): ?>
          <?php print render($link_info['content']); ?>
      <?php endforeach; ?>
    </div>
  </div>
<?php endforeach; ?>
