<?php

/**
 * @file
 * Template for widget page.
 */
?>

<div class="panel-amo-widget-group">
  <?php print render($content); ?>
</div>
