<?php

/**
 * @file
 * Template for widget html.
 */
?>

<!DOCTYPE html>
<html style="height: auto">
  <head lang="en">
    <meta charset="UTF-8">
    <?php print render($css); ?>
    <?php print render($js); ?>
  </head>
  <body>
    <?php print render($content); ?>
  </body>
</html>
