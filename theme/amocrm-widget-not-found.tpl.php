<?php

/**
 * @file
 * Template for widget not found.
 */
?>

<div style="text-align: center; margin: 0px 12px 0px 12px;padding-top: 66px;color: #6F7680;">
  <?php print t('Not found any widget for this page.'); ?>
</div>
