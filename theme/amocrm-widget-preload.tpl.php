<?php

/**
 * @file
 * Template for preload.
 */
?>

<div id="<?php print drupal_html_id('panel-amo-widget-preload'); ?>" style="text-align: center; padding-top: 67px;">
  <?php print render($preloader); ?>
</div>
